# Aplicação para análise de crédito
Aplicação web que realiza o cadastro de proposta de crédito para um determinado cliente, efetua a análise de dados e efetiva a aprovação ou negação de um limite de crédito para o mesmo.

## Aplicações
 - [analise-credito](analise-credito/) - módulo web com spring boot provendo os cadastro e visualizações
 - [analise-credito-core](analise-credito-core/) - módulo com spring boot provendo a análise de cŕedito
